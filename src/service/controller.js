const path = require('path'),
  fileLocation = path.join(__dirname, 'config.json'),
  authFileLocation = path.join(__dirname, 'auth.config.json'),
  backupFile = path.join(__dirname, 'config.json.bak'),
  fs = require('fs'),
  logger = require('./logger')

/** 
 * Función utilitaria
 * @returns {Object}
*/
function get_config_json() {
  try {
    return JSON.parse(fs.readFileSync(fileLocation, 'utf8'))
  } catch(error) {
    logger.error('get_config_error', error)
    return null
  }
}

exports.config_to_view = function() {
  const data = get_config_json()
  let viewConfig = {}
  for (let page of data.pages) {
    viewConfig[page.identifier] = page.formProps.map((formProp, index) => {
      let formData = {}
      formProp.rows.forEach( row => {
        row.cols.forEach( (col) => {
          const indexOfNumber = col.name.indexOf(index + 1)
          formData[col.name.substring(indexOfNumber + 1, col.name.length)] = col.value
        })
      })
      return formData
    })
  }
  return viewConfig
}

exports.auth = function(req, res) {
  try {
    const auth = JSON.parse(fs.readFileSync(authFileLocation, 'utf8'))
    if (req.body.user == auth.user && req.body.pass == auth.pass) {
      res.status(200).send()
    } else {
      res.status(401).send()
    }
  } catch (error) {
    logger.error('auth:: error', error)
    res.send(error)
  }
}

/**
 * Función de ruta
 * @param {*} req 
 * @param {*} res 
 */
exports.get_config = function(req, res) {
  try {
    res.json({
      data: get_config_json(),
    })
  } catch (error) {
    logger.error('get_config:: error', error)
  }
}

/**
 * Función de ruta
 * @param {*} req 
 * @param {*} res 
 */
exports.write_config = (req, res) => {
  try {
    if (!req.body || req.body == {})
      res.status(400).send("Bad Request")
    else {
      // Asquerosidad que va a buscar las imágenes y la elimina del JSON por temas 
      // de peso del archivo
      for (let pageStructure of req.body.pages) { // EUGH
        for (let formProp of pageStructure.formProps) { // AAAAGAH
          for (let row of formProp.rows) { // CANCEEEER
            for (let col of row.cols) { // NOOOO
              if (col.type == 0 && col.value) { // MGGGJJFJFFSSGGGFFFAA
                // Acá ocurre la magia, reemplaza la cabecera base64 por una cadena vacía
                let base64Data = col.value.replace(/^data:image\/[a-z]+;base64,/, ""),
                  binaryData = new Buffer(base64Data, 'base64').toString('binary');
                delete col.value // Elimina la propiedad del objeto

                // Si el archivo existe lo renombra (no puede sobreescribir directamente)
                if (fs.existsSync(path.join(__dirname, 'images', col.name))) {
                  fs.rename(path.join(__dirname, 'images', col.name, path.join(__dirname, 'images', 'oldest')))
                }
                // Escritura de archivo
                fs.writeFile(path.join(__dirname, 'images', col.name), binaryData, { encoding: 'binary', flag: 'wx' }, err => { console.error(err)})
              }
            }
          }
        }
      }

      // Escritura de archivo
      fs.writeFile(fileLocation, JSON.stringify(req.body),'utf8', (err, data) => {
        logger.log('write_config:: success')
        if (err) throw err
        else
          res.status(200).send()
      })
    }
      
  } catch (err) {
    logger.error(`write_config:: error`, err)
    // Retornar error
    res.send(err)
  }
}