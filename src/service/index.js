var express = require('express'),
app = express(),
port = process.env.PORT || 3000,
bodyParser = require('body-parser'),
path = require('path'),
routes = require('./routes')
ridentPath = path.join(__dirname, '../pages/rident-landing'),
ridentController = require('./controller')

// PUG viewEngine
app.set('view engine', 'pug')

// CORS - Middleware
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next()
})

// BodyParser
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ limit: '50mb' })) // Lìmite a 50 mb

// Rutas de API
routes(app, express)

// Rutas de Rident
app.get('/', (req, res) => {
  /**
   * ! Este valor está en duro, en algún momento será dinámico
   */
  const config = ridentController.config_to_view()
  res.render(path.join(ridentPath, 'src/views/index'), {
    config: config,
    imagePath: 'http://localhost:3000/images/',
  })
})

app.use('/common', express.static(path.join(ridentPath, '/common')))
app.use('/styles', express.static(path.join(ridentPath, '/dist/styles')))

// Imàgenes de meeta
app.use('/images', express.static(path.join(__dirname, '/images')))


app.listen(port)
console.log(`Magic happening on ${port}`)
