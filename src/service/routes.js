const path = require('path')

/**
 * Rutas
 * @param {Express} app 
 */
module.exports = function(app, express) {
  const config = require('./controller')

  app.route('/api')
    .get(config.get_config)
    .post(config.write_config)

  app.get('/meeta', (req, res) => {
    res.render(path.join(__dirname, '../pages/meeta'))
  })

  app.use('/meeta/assets', express.static(path.resolve(__dirname, '../pages', 'assets')))

  app.route('/api/auth')
    .post(config.auth)
}