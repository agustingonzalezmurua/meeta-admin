// Aplicacion para crear rápidamente archivos de configuración vírgenes
const path = require('path'),
fs = require('fs'),
yaml = require('js-yaml'),
inputFilePath = path.join(__dirname, 'input.yml'),
outputFilePath = path.join(__dirname, 'output.json')

try {
  console.log('Leyendo', inputFilePath)
  const doc = yaml.safeLoad(fs.readFileSync(inputFilePath, 'utf8'))
  let output = {
    clientName: doc.clientName,
    pages: doc.pages.map(page => {
      let pageStructure = { 
        identifier: page.identifier,
        formProps: []
      }
      for (let i = 1; i <= page.entryAmount; i++) {
        let rows = []
        page.formProps.colStructure.forEach(colStructure => {
          let columns = []
          Object.keys(colStructure).forEach(propName => {
            const colDefinition = colStructure[propName]
            columns.push({
              name: `${page.formProps.subTitle.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase()}${i}${propName}`,
              label: colDefinition.label,
              type: colDefinition.type,
              placeholder: colDefinition.placeholder,
              width: colDefinition.width
            })
          })
          rows.push({ "cols": columns })
        })
        pageStructure.formProps.push({
          sub: `${page.formProps.subTitle} #${i}`,
          rows: rows
        })
      }
      return pageStructure
    }),
    url: doc.url
  }

  fs.writeFile(outputFilePath, JSON.stringify(output), 'utf8')
} catch (error) {
  console.error(error)
}