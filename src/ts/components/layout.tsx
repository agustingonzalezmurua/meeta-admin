import * as React from 'react'
import Button, { ButtonKinds, ButtonColors, ButtonSizes } from '@ts/components/button'
import '@styles/meeta-admin-styles'

interface IMainProps {
  clientName: string
  menuStructure: MenuEntry[]
  selectedIdentifier: string
}

export class Main extends React.Component<IMainProps, any> {
  render() {
    return (
      [
      <span className="meeta-logo page">me<span className="highlight">e</span>ta</span>,
      <Center>
        <div className="meeta-page meeta-col">
          <div className="meeta-wrapper main width-960px title lg">
            <h1 className="meeta-h1 width-960px lg meeta-row between">
              &nbsp;PANEL DE ADMINISTRACIÓN - {this.props.clientName}
              <span className="meeta-linkeable">
                <svg className="svg-inline--fa fa-sign-out-alt fa-w-16" aria-hidden="true" data-fa-processed="" data-prefix="fas" data-icon="sign-out-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                  <path fill="currentColor" d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z">
                  </path>
                </svg>
                <i className="fas fa-sign-out-alt" />
                Salir
            </span>
            </h1>
            <div className="meeta-row top">
              <Menu entries={this.props.menuStructure} selectedIdentifier={this.props.selectedIdentifier}/>
              <Col>
                <Scrollable>
                  {this.props.children}
                </Scrollable>
              </Col>
            </div>
          </div>
        </div>
      </Center>
      ]
    )
  }
}

export class Center extends React.Component {
  render() {
    return <div className="meeta-wrapper-center">{ this.props.children }</div>
  }
}

export class Wrapper extends React.Component<IDimensions, any> {
  render() {
    return <div className={`meeta-wrapper dark ${dimension(this.props)}`}>{ this.props.children}</div>
  }
}

interface IMenuProps {
  selectedIdentifier: string
  entries: MenuEntry[]
}

export class MenuEntry {
  constructor(text:string, onClick: Function) {
    this.text = text
    this.onClick = onClick
  }
  text: string
  onClick: Function
}

export class Menu extends React.Component<IMenuProps, any> {
  render() {
    return (
      <nav className="meeta-main-menu meeta-column start width-120px spacer-right">
        {
          this.props.entries.map((menuEntry, index) =>
            {
              return (
                <ElementContainer width="100" key={index} >
                  <Button
                    color={ButtonColors.light}
                    size={ButtonSizes.sm}
                    kind={ButtonKinds.square}
                    onClick={menuEntry.onClick}
                    active={this.props.selectedIdentifier == menuEntry.text}
                  >
                    {menuEntry.text}
                  </Button>
                </ElementContainer>)
            }
          )
        }
      </nav>
    )
  }
}

export class Col extends React.Component {
  render() {
    return <div className="meeta-wrapper-col">{ this.props.children } </div>
  }
}

export class Row extends React.Component {
  render() {
    return <div className="meeta-row start">{ this.props.children }</div>
  }
}

export class Scrollable extends React.Component {
  render() {
    return <div className="meeta-scroll">{ this.props.children }</div>
  }
}

interface IDimensions {
  width?: number|string
  height?: number|string
}
export class ElementContainer extends React.Component<IDimensions, any> {
  render() {
    return(
      <div className={`meeta-element-container ${dimension(this.props)}`}>
        { this.props.children }
      </div>
    )
  }
}

function dimension(props: any) {
  if (props.width) {
    return `width-${props.width}`
  }

  if (props.height) {
    return `height-${props.height}`
  }
}