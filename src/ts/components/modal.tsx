import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { ReactElement } from 'react'
import { Editor } from 'react-draft-wysiwyg'
import InputFile from '@ts/components/input-file'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'
import { EditorState, convertToRaw, ContentState } from 'draft-js'

import '@styles/meeta-admin-modal'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

interface IModalProps {
  type: string
  size: string
  inputRef?: any
  title: string
  onClose: Function
}

interface IToolbarOptions {
  options: Array<string>
  inline: any
}

const toolbarOptions: IToolbarOptions = {
  'options': ['inline', 'textAlign', 'list', 'blockType'],
  'inline': {
    'options': ['bold', 'italic', 'underline']
  }
}

export default class Modal extends React.Component<IModalProps, any> {
  input:any
  refNode: HTMLElement = document.getElementById('modal')

  constructor(props: IModalProps) {
    super(props)

    this.state = {
      editorState: EditorState.createWithContent(ContentState.createFromBlockArray(htmlToDraft(this.props.inputRef.value)))
    }

    this.handleClose = this.handleClose.bind(this)
    this.getInput = this.getInput.bind(this)
    this.handleEditorStateChange = this.handleEditorStateChange.bind(this)
  }

  handleEditorStateChange(editorState: any) {
    this.setState({ editorState })
  }

  getInput(type: IModalProps["type"]):any {
    switch (type) {
      case 'image':
        return (
          [
            <p className="meeta-modal-p">
              &nbsp;Te recomendamos usar una resolución superior a 800px y usar tinypng.com para bajar el peso de tus imágenes
            </p>,
            <div className="meeta-modal-img-container">
              <div className="meeta-placeholder-img" onClick={() => this.props.inputRef.click()}>
                Cambiar imágen
              </div>
            </div>
          ]
        )
      case 'textArea':
        const { editorState } = this.state
        return (
          [
            <p className="meeta-modal-p">
              &nbsp;Te recomendamos no llenar de texto tu sitio web para no afectar la usabilidad. Los visitantes leen alrededor de un 20% del texto total y quienes estan muy interesados no alcanzan a leer más allá del 60% de un artículo. Si quieres mejor posicionamiento se escueto y utiliza las palabras clave dentro de tu redacción.
            </p>,
            <div className="meeta-modal-textarea-container">
              <Editor
                toolbar={toolbarOptions}
                editorState={editorState}
                onEditorStateChange={this.handleEditorStateChange}
              />
            </div>
          ]
        )
      case 'text':
        return(
          [
            <p className="meeta-modal-p">
              &nbsp;Si los títulos tienen demasiados caracteres pueden romper el diseño. Pero si sientes que necesitas más flexibilidad reportalo como issue a<a href="mailto:soporte@meeta.cl">&nbsp;soporte@meeta.cl</a>
            </p>,
            <textarea autoFocus className="meeta-textarea dark" ref={(ref:any) => this.input = ref} defaultValue={this.props.inputRef.value}/>
          ]
        )
    }
  }

  handleClose(provideData?: boolean) {
    if (provideData) {
      let value
      switch (this.props.type) {
        case 'image': // Cambia valores por referencia
          break;
        case 'text': // Texto simple
          value = this.input.value
          break;
        case 'textArea':
          value = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
          break;
        default:
          value = null
          break;
      }
      this.props.onClose(value)
    } else
      this.props.onClose()
  }

  render() {
    return(
      ReactDOM.createPortal(
        <div className="meeta-modal-bg">
          <div className={`meeta-modal-container ${this.props.type}`}>
            <div className={`meeta-modal ${this.props.size}`}>
              <div className="meeta-modal-header">
                <h4 className="meeta-modal-h4">
                  {this.props.title}
                </h4>
              </div>
              <div className="meeta-modal-body">
                {
                  this.getInput(this.props.type)
                }
              </div>
              <div className="meeta-modal-footer">
                <form className="meeta-form">
                  <div className="meeta-row end">
                    <div className="meeta-element-container width-140px">
                      <button className="meeta-button sky sm close-modal" type="button" onClick={() => this.handleClose(false)} >Cancelar</button>
                    </div>
                    <div className="meeta-element-container width-140px">
                      <button className="meeta-button sky sm" type="button" onClick={() => this.handleClose(true)}>Guardar</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>,
        this.refNode
      )
    )
  }
}