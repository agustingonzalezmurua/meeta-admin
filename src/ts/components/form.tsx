import * as React from 'react'
import { Row, ElementContainer } from '@ts/components/layout';
import InputFile from '@ts/components/input-file'
import Input from '@ts/components/input'
import Button, { ButtonColors, ButtonSizes, ButtonKinds } from '@ts/components/button'
import '@styles/meeta-admin-form'
import { SyntheticEvent } from 'react';
import Modal from '@ts/components/modal'
import InputTextArea from '@ts/components/input-textarea'
const md5 = require('md5')

export interface IFormProps {
  sub?: string
  rows: IFormRow[]
  onSubmit?: (inputs: FormEntry[]) => void
}
interface IFormRow {
  cols: IFormCol[]
}

interface IFormCol {
  label: string
  type: entryTypes
  name: string
  width?: string|number
  placeholder?: string
  value?: any
  /** Automáticamente seteado */
  key?: string
}

export class FormEntry {
  type: entryTypes
  name: string
  value: any
  rowIndex:number
  colIndex:number
  constructor(type:entryTypes, name:string, value:any) {
    this.type = type
    this.name = name
    this.value = value
  }
}

enum entryTypes {
  image,
  text,
  textArea
}

function findLocation(nameToFind: string, pops:IFormProps) {
  const rowIndex = pops.rows.findIndex(row => row.cols.find(col => col.name == nameToFind) != undefined )
  const colIndex = pops.rows[rowIndex].cols.findIndex(col => col.name == nameToFind)

  return { rowIndex, colIndex }
}

export default class Form extends React.Component<IFormProps, any> {
  constructor(props:any) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  form: HTMLFormElement

  formGroup(entries: IFormRow[]) {
    return this.props.rows.map((formGroup,index) =>
      <Row key={index}>
        {
          formGroup.cols.map((entry, index) => this.formEntry(entry))
        }
      </Row>
    )
  }

  formEntry(entry: IFormCol):JSX.Element {
    let entryComponent
    switch (entry.type) {
      case entryTypes.image:
        entryComponent = <InputFile {...entry}/>
        break;
      case entryTypes.text:
        entryComponent = <Input {...entry}/>
        break;
      case entryTypes.textArea:
        entryComponent = <InputTextArea {...entry} />
        break;
      default:
        entryComponent = null
        break;
    }
    return (
      <ElementContainer width={entry.width} key={'container' + entry.name}>
        {<label className="meeta-label dark">{entry.label}:</label>}
        {entryComponent}
      </ElementContainer>
    )
  }

  handleSubmit(e: any) {
    const inputs: FormEntry[] = []
    for (let i = 0; i < this.form.elements.length; i++) {
      const element: any = this.form.elements[i]
      if (element.tagName == 'INPUT' && element.name) {
        let formEntry: FormEntry
        switch (element.dataset.type) {  
          case 'file':
            formEntry = new FormEntry(entryTypes.image, element.name, element.value)
            break;
          case 'text':
            formEntry = new FormEntry(entryTypes.text, element.name, element.value)
            break;
          case 'textArea':
            formEntry = new FormEntry(entryTypes.textArea, element.name, element.value)
            break;
          default:
            break
        }

        const location = findLocation(element.name, this.props)
        inputs.push(Object.assign({}, formEntry, location))
      }
    }

    if (this.props.onSubmit)
      this.props.onSubmit(inputs)
  }

  render() {
    return(
      <div className="meeta-wrapper dark width-100">
        <form className="meeta-form meeta-col start" ref={form => this.form = form}>
          { // Subtítulo
            this.props.sub ?
              <div className="meeta-element-container width-100">
                <span className="meeta-sub">{this.props.sub}</span>
              </div>
              : null
          }
          { // Entradas de datos
            this.formGroup(this.props.rows)
          }
          <ElementContainer width="140px">
            <Button color={ButtonColors.sky} onClick={this.handleSubmit} size={ButtonSizes.sm}>Guardar</Button>
          </ElementContainer>
        </form>
      </div>
    )
  }
}
