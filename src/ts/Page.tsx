import * as React from 'react'
import * as Layout from '@ts/components/layout'
import { MenuEntry } from '@ts/components/layout'
import Form, { IFormProps, FormEntry } from '@ts/components/form'
import { IPageContainerState } from '@ts/App';

export interface IPageStructure {
  identifier: string
  formProps: IFormProps[]
}

export class Page extends React.Component<IPageContainerState & { onSubmit: (inputs: FormEntry[], propIndex: number, ) => void }, any> {
  render() {
    return (
      <Layout.Main menuStructure={this.props.structure.menu} clientName={this.props.structure.clientName} selectedIdentifier={this.props.selectedPageIdentifier}>
        {
          this.props.structure.pages.find(page => page.identifier == this.props.selectedPageIdentifier).formProps.map((formProps, index) => {
            return (
              <Form
                {...formProps}
                onSubmit={(inputs: FormEntry[]) => { this.props.onSubmit(inputs, index) }}
              />
            )
          }
          )
        }
      </Layout.Main>
    )
  }
}
