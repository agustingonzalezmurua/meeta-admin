import * as React from 'react'
import * as ReactDOM from 'react-dom'
import '@styles/normalize.css'
import '@styles/font-awesome.css'
import { IPageStructure, Page } from '@ts/Page'
import { MenuEntry } from '@ts/components/layout'
import { FormEntry, IFormProps } from '@ts/components/form'
import Login from '@ts/Login';
import *  as md5 from 'js-md5'
import { ToastContainer, toast } from 'react-toastify'

/** Clase auxiliar de mapeado de propiedades para crear el menú */
class AppStructure implements IAppConfigStructure {
  clientName: string;
  url: string
  pages: IPageStructure[]
  menu: MenuEntry[]

  constructor(props: IAppConfigStructure, setSelectedIdentifier: Function) {
    this.clientName = props.clientName,
    this.pages = props.pages
    this.url = props.url
    this.menu = props.pages.map(structure => new MenuEntry(structure.identifier, () =>  setSelectedIdentifier(structure.identifier) ))
  }
}

export interface IAppConfigStructure {
  clientName: string
  url: string
  pages: IPageStructure[]
  menu?: MenuEntry[],
}

export interface IPageContainerState {
  selectedPageIdentifier: string
  structure: AppStructure,
  authenticated: boolean
}

/** Contenedor de funcionalidad */
class PageContainer extends React.Component<IAppConfigStructure, IPageContainerState> {

  constructor(props: IAppConfigStructure) {
    super(props)
    
    this.onSubmit = this.onSubmit.bind(this)
    this.setSelectedIdentifier = this.setSelectedIdentifier.bind(this)
    this.sendData = this.sendData.bind(this)
    this.authenticate = this.authenticate.bind(this)

    let structure = new AppStructure(this.props, this.setSelectedIdentifier )

    this.state = {
      selectedPageIdentifier: structure.pages[0].identifier,
      structure,
      authenticated: false
    }
  }

  onSubmit(inputs: FormEntry[], formPropIndex: number, selectedIdentifier: string = this.state.selectedPageIdentifier): void {
    const selectedPageInstance = this.state.structure.pages.find(page => page.identifier == selectedIdentifier)
    const updatedFormProps = this.getUpdatedFormProp(selectedPageInstance.formProps[formPropIndex], inputs)
    const indexOfSelectedPage = this.state.structure.pages.findIndex(page => page.identifier == selectedIdentifier)

    let structure = this.state.structure
    structure.pages[indexOfSelectedPage] = Object.assign({}, structure.pages[indexOfSelectedPage], selectedPageInstance)
    this.sendData(structure)
  }

  sendData(newStructure: IAppConfigStructure) {
    const apiCopy = Object.assign({}, newStructure, { menu: null})
    fetch(this.props.url, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(apiCopy),
      method: 'POST',
    }).then(response => {
      // Cambio aceptado
      if (response.ok) {
        let structure = this.state.structure;
        structure.pages = newStructure.pages
        this.setState({ structure: structure })
        toast('Datos actualizados exitosamente', { type: 'success' })
      } else {
        toast('Oops! Ocurrió un error al intentar actualizar los datos', { type: 'error' })
      }
    }).catch(error => console.error(error))
  }

  setSelectedIdentifier(identifier: string) {
    this.setState({ selectedPageIdentifier: identifier })
  }

  authenticate(user:string, pass:string) {
    const payload = {
      user,
      pass: md5.create().update(pass).hex()
    }

    fetch(this.props.url + '/auth', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload),
      method: 'POST',
    }).then(response => {
      if (response.ok) {
        this.setState({ authenticated: true })
      } else {
        toast('Credenciales inválidas', { type: 'error'})
      }
    })
  }

  getUpdatedFormProp(formProps: IFormProps, entries: FormEntry[]): IFormProps {
    console.log(formProps, entries)
    let output: IFormProps = Object.assign({}, formProps)
    for (let formEntry of entries) {
      output.rows[formEntry.rowIndex].cols[formEntry.colIndex].value = formEntry.value
    }
    return output
  }

  render() {
    if (!this.state.authenticated) return <div><Login authenticate={this.authenticate}/><ToastContainer /></div>
    return ([
      <Page { ...this.state } onSubmit={this.onSubmit} key="appPage"/>,
      <ToastContainer />
    ])
  }
}

const testConfig: IAppConfigStructure = {
  clientName: 'Rident',
  url: 'http://localhost:3000/api',
  pages: [
    { // Especialidades
      identifier: 'Especialidades',
      formProps: [
        { // Especialidad 1
          sub: 'Especialidad #1',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad1icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad1Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad1texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad1icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad1Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad1Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 2
          sub: 'Especialidad #2',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad2icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad2Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad2texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad2icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad2Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad2Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 3
          sub: 'Especialidad #3',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad3icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad3Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad3texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad3icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad3Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad3Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 4
          sub: 'Especialidad #4',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad4icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad4Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad4texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad4icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad4Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad4Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 5
          sub: 'Especialidad #5',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad5icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad5Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad5texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad5icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad5Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad5Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 6
          sub: 'Especialidad #6',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad6icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad6Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad6texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad6icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad6Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad6Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 7
          sub: 'Especialidad #2',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad7icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad7Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad7texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad7icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad7Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad7Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 8
          sub: 'Especialidad #8',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad8icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad8Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad8texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad8icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad8Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad8Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 9
          sub: 'Especialidad #9',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad9icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad9Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad9texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad9icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad9Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad9Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
        { // Especialidad 10
          sub: 'Especialidad #10',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'especialidad10icono',
                  label: 'Ícono',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad10Titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad10texto',
                  label: 'Texto',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 2,
                  width: '400px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'especialidad10icono',
                  label: 'Texto Botón',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '140px'
                },
                {
                  name: 'especialidad10Titulo',
                  label: 'Título',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'especialidad10Descripción',
                  label: 'Texto',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 2,
                  width: '400px'
                },
              ]
            }
          ]
        },
      ]
    },
    { // Promociones
      identifier: 'Promociones',
      formProps: [
        { // Promoción 1
          sub: 'Promoción #1',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'promocion1imagen',
                  label: 'Imágen',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '240px'
                },
                {
                  name: 'promocion1titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '240px'
                },
                {
                  name: 'promocion1subtitulo',
                  label: 'Subtítulo',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 1,
                  width: '240px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'promocion1texto',
                  label: 'Texto',
                  type: 2,
                  placeholder: 'Blanqueamiento led + limpieza dental con ultrasonido profilaxis + IHO + diagnóstico',
                  width: '400px'
                },
                {
                  name: 'promocion1textoboton',
                  label: 'Texto Botón',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'promocion1enlaceboton',
                  label: 'Enlace Botón',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 1,
                  width: '180px'
                },
              ]
            }
          ]
        },
        { // Promoción 2
          sub: 'Promoción #1',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'promocion2imagen',
                  label: 'Imágen',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '240px'
                },
                {
                  name: 'promocion2titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '240px'
                },
                {
                  name: 'promocion2subtitulo',
                  label: 'Subtítulo',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 1,
                  width: '240px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'promocion2texto',
                  label: 'Texto',
                  type: 2,
                  placeholder: 'Blanqueamiento led + limpieza dental con ultrasonido profilaxis + IHO + diagnóstico',
                  width: '400px'
                },
                {
                  name: 'promocion2textoboton',
                  label: 'Texto Botón',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'promocion2enlaceboton',
                  label: 'Enlace Botón',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 1,
                  width: '180px'
                },
              ]
            }
          ]
        },
        { // Promoción 3
          sub: 'Promoción #3',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'promocion3imagen',
                  label: 'Imágen',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '240px'
                },
                {
                  name: 'promocion3titulo',
                  label: 'Título',
                  placeholder: 'ej, Endodoncia',
                  type: 1,
                  width: '240px'
                },
                {
                  name: 'promocion3subtitulo',
                  label: 'Subtítulo',
                  placeholder: 'ej, Tratamiento para reparar y salvar un diente gravemente daña...',
                  type: 1,
                  width: '240px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'promocion3texto',
                  label: 'Texto',
                  type: 2,
                  placeholder: 'Blanqueamiento led + limpieza dental con ultrasonido profilaxis + IHO + diagnóstico',
                  width: '400px'
                },
                {
                  name: 'promocion3textoboton',
                  label: 'Texto Botón',
                  placeholder: 'imagen-boton.jpg',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'promocion3enlaceboton',
                  label: 'Enlace Botón',
                  placeholder: 'ej, La endodoncia también conocido como tratamiento de conducto, es la rama de la odontología que realiza el tratamient...',
                  type: 1,
                  width: '180px'
                },
              ]
            }
          ]
        },
      ]
    },
    { // Nosotros
      identifier: 'Nosotros',
      formProps: [
        { // Nosotros 1
          sub: 'Nosotros #1',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'nosotros1imagen',
                  label: 'Foto',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '180px'
                },
                {
                  name: 'nosotros1nombre',
                  label: 'Nombre',
                  placeholder: 'ej, Juan Pérez',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros1especialidad',
                  label: 'Especialista en',
                  placeholder: 'ej, Algo',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros1sede',
                  label: 'Atiende en',
                  placeholder: 'ej, Aquella sede',
                  type: 1,
                  width: '180px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'nosotros1texto',
                  label: 'Frase inspiradora, recomendación, etc',
                  type: 2,
                  placeholder: 'ej, frase inspiradora de este especialista...',
                  width: '720px'
                },
              ]
            }
          ]
        },
        { // Nosotros 2
          sub: 'Nosotros #2',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'nosotros2imagen',
                  label: 'Foto',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '180px'
                },
                {
                  name: 'nosotros2nombre',
                  label: 'Nombre',
                  placeholder: 'ej, Juan Pérez',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros2especialidad',
                  label: 'Especialista en',
                  placeholder: 'ej, Algo',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros2sede',
                  label: 'Atiende en',
                  placeholder: 'ej, Aquella sede',
                  type: 1,
                  width: '180px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'nosotros2texto',
                  label: 'Frase inspiradora, recomendación, etc',
                  type: 2,
                  placeholder: 'ej, frase inspiradora de este especialista...',
                  width: '720px'
                },
              ]
            }
          ]
        },
        { // Nosotros 3
          sub: 'Nosotros #3',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'nosotros3imagen',
                  label: 'Foto',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '180px'
                },
                {
                  name: 'nosotros3nombre',
                  label: 'Nombre',
                  placeholder: 'ej, Juan Pérez',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros3especialidad',
                  label: 'Especialista en',
                  placeholder: 'ej, Algo',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros3sede',
                  label: 'Atiende en',
                  placeholder: 'ej, Aquella sede',
                  type: 1,
                  width: '180px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'nosotros3texto',
                  label: 'Frase inspiradora, recomendación, etc',
                  type: 2,
                  placeholder: 'ej, frase inspiradora de este especialista...',
                  width: '720px'
                },
              ]
            }
          ]
        },
        { // Nosotros 4
          sub: 'Nosotros #4',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'nosotros4imagen',
                  label: 'Foto',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '180px'
                },
                {
                  name: 'nosotros4nombre',
                  label: 'Nombre',
                  placeholder: 'ej, Juan Pérez',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros4especialidad',
                  label: 'Especialista en',
                  placeholder: 'ej, Algo',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros4sede',
                  label: 'Atiende en',
                  placeholder: 'ej, Aquella sede',
                  type: 1,
                  width: '180px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'nosotros4texto',
                  label: 'Frase inspiradora, recomendación, etc',
                  type: 2,
                  placeholder: 'ej, frase inspiradora de este especialista...',
                  width: '720px'
                },
              ]
            }
          ]
        },
        { // Nosotros 5
          sub: 'Nosotros #5',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'nosotros5imagen',
                  label: 'Foto',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '180px'
                },
                {
                  name: 'nosotros5nombre',
                  label: 'Nombre',
                  placeholder: 'ej, Juan Pérez',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros5especialidad',
                  label: 'Especialista en',
                  placeholder: 'ej, Algo',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros5sede',
                  label: 'Atiende en',
                  placeholder: 'ej, Aquella sede',
                  type: 1,
                  width: '180px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'nosotros5texto',
                  label: 'Frase inspiradora, recomendación, etc',
                  type: 2,
                  placeholder: 'ej, frase inspiradora de este especialista...',
                  width: '720px'
                },
              ]
            }
          ]
        },
        { // Nosotros 6
          sub: 'Nosotros #6',
          rows: [
            { // Fila 1
              cols: [
                {
                  name: 'nosotros6imagen',
                  label: 'Foto',
                  type: 0,
                  placeholder: 'image-ejemplo.jpg',
                  width: '180px'
                },
                {
                  name: 'nosotros6nombre',
                  label: 'Nombre',
                  placeholder: 'ej, Juan Pérez',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros6especialidad',
                  label: 'Especialista en',
                  placeholder: 'ej, Algo',
                  type: 1,
                  width: '180px'
                },
                {
                  name: 'nosotros6sede',
                  label: 'Atiende en',
                  placeholder: 'ej, Aquella sede',
                  type: 1,
                  width: '180px'
                },
              ],
            },
            { // FIla 2
              cols: [
                {
                  name: 'nosotros6texto',
                  label: 'Frase inspiradora, recomendación, etc',
                  type: 2,
                  placeholder: 'ej, frase inspiradora de este especialista...',
                  width: '720px'
                },
              ]
            }
          ]
        },
      ]
    },
  ]
}

export function render(config?: IAppConfigStructure) {
  if (!config)
    config = testConfig
  ReactDOM.render(React.createElement(PageContainer, config), document.getElementById('app'))
}
